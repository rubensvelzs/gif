import { useContext } from "react";
import { useNavigate } from "react-router-dom";
import { AuthContext } from "../auth/context";
import { IUserNavigationProps } from "../types";


export const useNavigation =  (): IUserNavigationProps[] => {
  const navigate = useNavigate();
  const {logout} = useContext(AuthContext);

  return[
    {
        id:1,
        action: () => navigate('/favorites'),
        label: "Favorites"
    },
    {
      id: 2,
      action: () => {logout(); navigate('/login')},
      label: "Logout"
    },
  ]
}
