import { useCallback, useState } from "react";
import { IGifResource } from "../services/gifs";

import { IBaseState } from "../types";

export type removeError=()=>void;

export type removeLoading=()=>void;

export type setError=()=>void;

export type setLoading=()=> void;

export const useGifPerformance=<T extends IGifResource>():[
    IBaseState,
    removeError,
    removeLoading,
    setError,
    setLoading
]=>{

    const [state, setState] = useState<IBaseState>({
        error:false,
        loading:false,
    });

    const removeError = useCallback(()=>{
        setState({...state,error:false})
    },[state]);

    const removeLoading = useCallback(()=>{
        setState({...state, loading:false});
    },[state])

    const setError = useCallback(()=>{
        setState({...state, error:true});
    },[state, setState])
    

    const setLoading = useCallback(()=>{
        setState({...state, loading:true});
    },[state, setState]);

    return [state, removeError, removeLoading, setError, setLoading]
}