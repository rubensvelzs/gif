
import { FormElement } from '@nextui-org/react';
import  { ChangeEvent, useState } from 'react';

export const useForm = <T extends Object>(initialState:T) => {

    const [state,setState]= useState<T>(initialState);

    const handleChange=({target}:ChangeEvent<FormElement>)=>{
        const {name,value}= target;
        setState({
         ...state,
         [name]:value
        })
       }

  return {...state, handleChange}
}