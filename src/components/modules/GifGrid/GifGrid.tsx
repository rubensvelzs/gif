import { Button, Grid, Loading } from "@nextui-org/react"
import { useContext } from "react";
import { IGifResource } from '../../../services/gifs/types.d';
import { GifCard } from "../../elements";
import {FavoritesContext} from '../../../favorites/context'
import { IBaseState } from "../../../types";



export interface IGifGridProps{
    /**
     * Source gifs array.
     */
    gifs: IGifResource[]
    /**
     * 
     */
    performance : IBaseState;
}

export const GifGrid:React.FC<IGifGridProps> = (props):JSX.Element => {
  const {gifs, performance} = props;

  if(performance?.loading){
    return(
      <div style={{display:"flex", justifyContent:"center"}}>
         <Loading color="secondary"/>
      </div>
       
    )
  
  }
  
  return (
    <Grid.Container gap={6} justify="center">
        {gifs?.map(item=>(
        <Grid key={item?.id} md={4}>
             <GifCard gif={item} />
        </Grid>
        ))}
    </Grid.Container>
  )
}
