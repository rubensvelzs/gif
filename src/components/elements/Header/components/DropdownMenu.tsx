
import { Avatar, Link, Text, Dropdown } from '@nextui-org/react';
import { useMemo } from 'react';
import { IUserNavigationProps, IUserResource } from '../../../../types';

export interface IDropdownMenuProps{
    /**
     * 
     */
    user:IUserResource;
    /**
     * 
     */
    userNavigation: IUserNavigationProps[]
}

export const DropdownMenu:React.FC<IDropdownMenuProps> = (props):JSX.Element => {
  const {user, userNavigation} = props;

  const avatar = useMemo(()=> user?.avatar,[user])

  return (
    <Dropdown placement="bottom-left">
            <Dropdown.Trigger>
              <div style={{alignItems:"center", cursor:"pointer", display:"flex"}}>
                <Avatar
                bordered
                size="lg"
                color="secondary"
                src={avatar}
                />

                <Text b color="inherit" css={{marginLeft:8}}>
                 {user?.email}
                </Text>
              </div>
            </Dropdown.Trigger>
           
            <Dropdown.Menu  color="secondary" aria-label="Avatar Actions" >
               {userNavigation?.map((item)=>(
                <Dropdown.Item color={item?.label==="Logout"?"error":"default"} key={item?.id} withDivider
                >
                  <Link color={item?.label==="Logout"?"error":"default"} onPress={item?.action}>
                    {item?.label}
                  </Link>
                </Dropdown.Item>
               ))}
            </Dropdown.Menu>
        </Dropdown>
  )
}
