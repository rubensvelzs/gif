import { Button } from "@nextui-org/react";

export interface IUserNavigationButtonProps{
    /**
     * Handle action when the button is clicked.
     */
    action:()=>void;
    /**
     * If `true`, the button only will display with border.
     */
    isBordered?:boolean;
    /**
     * Button's label.
     */
    label:string;
}

export const UserNavigationButton:React.FC<IUserNavigationButtonProps> =
 (props):JSX.Element => {
  const {action, isBordered, label} = props;
    
  return (
    <Button auto bordered={isBordered} color="gradient" onPress={action}>
        {label}
    </Button>
  )
}
