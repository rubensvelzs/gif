import {Link,Text, useTheme } from '@nextui-org/react';
import { useNavigate } from 'react-router-dom';
import { INavigationProps, IUserNavigationProps, IUserResource } from '../../../types';
import { DropdownMenu } from './components/DropdownMenu';
import { UserNavigationButton } from './components/UserNavigationButton';

export interface IHeaderProps{
  /**
  * 
  */
  routes?: INavigationProps[];
  /**
  * Header's principal title.
  */
  title?:string;
  /**
   * 
   */
  user: IUserResource;
  /**
   * 
   */
  userNavigation: IUserNavigationProps[];
}

export const Header:React.FC<IHeaderProps> = (props):JSX.Element => {
  const {title="Gif App", routes, user, userNavigation, ...rest}= props;

  const {theme}= useTheme();
  const navigate = useNavigate();
 
  return (
    <div style={{ 
        alignItems:"center",
        backgroundColor:theme?.colors.gray100.value,
        display:"flex",
        flexDirection:"row",
        justifyContent:"space-between",
        marginBottom:"30px",
        padding:"0px 20px",
        width:"100%",
      }}
        >
          <Link onPress={()=>{navigate('/')}}>
            <Text h1 css={{ textGradient: "45deg, $blue600 -20%, $pink600 50%",}}
               weight="bold">{title}
            </Text>
          </Link>
       

          {user?( <DropdownMenu user={user} userNavigation={userNavigation}/>):
          <UserNavigationButton action={()=>navigate('/login')} label="Login"/>}
        </div>
  )
}
