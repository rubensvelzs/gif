import { Button, Card, Link, Loading,  Text } from "@nextui-org/react"
import { useContext, useState } from "react";
import { FavoritesContext } from "../../../favorites/context";
import { IGifResource } from "../../../services/gifs";
import { BsHeart, BsHeartFill} from 'react-icons/bs';
import { IBaseState, IFavoriteResource } from "../../../types";

export interface IGifCardProps extends React.CSSProperties {
   /**
    * 
    */
   gif: IGifResource;

}

export const GifCard:React.FC<IGifCardProps> = (props):JSX.Element => {
  const {gif}= props;
  const {addFavorite, removeFavorite}= useContext(FavoritesContext);

  const [like, setLike]= useState<boolean>(false);

  const onAddFavorite=()=>{
    addFavorite({...gif});
    setLike(true);
  }

  const onRemoveFavorite=(favoriteId:string)=>{
    removeFavorite(favoriteId);
    setLike(false);
  }


  return (
      <Card css={{ marginBottom:"12px", width:"100%"}}>
       <Card.Image
          alt="Card image background"
          height={340}
          objectFit="cover"
          src={gif?.images?.downsized_medium?.url as string}
          width="100%"
        />
        <Card.Footer>
        <div style={{ alignItems:"center"}}>
          {like?( <Link css={{ marginBottom:"8px"}} onPress={()=>onRemoveFavorite(gif?.id)}>
            <BsHeartFill color="red" size={22}/>
          </Link>):( <Link css={{ marginBottom:"8px"}} onPress={onAddFavorite}>
            <BsHeart color="red" size={22}/>
          </Link>)}
          <Text size="$sm" h5>{gif?.title}</Text>
        </div>
        </Card.Footer>
      </Card>
  )
}
