import {Button, FormElement, Input } from '@nextui-org/react'

export interface ISearchFormProps{
    handleChange:((e: React.ChangeEvent<FormElement>) => void);
    onSubmit:()=>void;
}

export const SearchForm:React.FC<ISearchFormProps> = (props):JSX.Element => {
    const {handleChange, onSubmit, ...rest}= props;

  return (
    <form style={{ alignItems:"center",display:"flex", justifyContent:"center", marginBottom:"20px"}}>
       <Input css={{mr:"10px"}} clearable name="name" onChange={handleChange} labelPlaceholder="Name" width="500px"/>
       <Button color="gradient" onPress={onSubmit}> Buscar </Button>
    </form>

  )
}
