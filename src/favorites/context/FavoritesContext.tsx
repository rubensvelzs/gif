import { createContext } from "react";
import { IGifResource } from "../../services/gifs";
import { IFavoriteResource } from "../../types";

export interface IFavoriteContextProps{
    state: IGifResource[],
    addFavorite: (gif: IGifResource)=>void;
    removeFavorite:(favoriteId:string)=>void;
}

export const FavoritesContext= createContext({} as IFavoriteContextProps);