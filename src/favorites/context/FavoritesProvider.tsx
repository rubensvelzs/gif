import { ReactElement, useReducer } from "react"
import { IGifResource } from "../../services/gifs";
import { IFavoriteResource } from '../../types';
import { favoritesReducer } from "../reducers";
import { FavoritesTypes } from "../types.d";
import { FavoritesContext } from "./FavoritesContext";



export interface IFavoritesProviderProps{
    /**
     * 
     */
    children: ReactElement;
}

export const FavoritesProvider:React.FC<IFavoritesProviderProps> = (props):JSX.Element => {
    const {children}= props;
    const initialState: IGifResource[]=[{
      id:"",
      images:{downsized_medium:{url:""}},
      username:"",
      title:""
    }]

    const [state, dispatch ]= useReducer(favoritesReducer, initialState as never);

    const favorites= JSON.parse(localStorage.getItem('favorites') as string) as IGifResource[];
    
    const isFavoriteExist=(id:string):boolean=>{
     const result= favorites?.some((item: { id: string; })=>item?.id===id);
     return result;
    } 

    const addFavorite=(favorite:IGifResource)=>{
      const action={
            type: FavoritesTypes.addFavorite,
            payload: favorite
        }
        const isDuplicated= isFavoriteExist(favorite?.id);

        if(!isDuplicated){
          dispatch(action);
          localStorage.setItem('favorites',JSON.stringify([...state as any, favorite]));
        }
     
    }

    const removeFavorite =(favoriteId:string)=>{

     const getFavorite= favorites.filter(item=>item.id!==favoriteId);
     localStorage.setItem('favorites',JSON.stringify(getFavorite));
    }


  return (
   <FavoritesContext.Provider value={{state , addFavorite, removeFavorite}}>
    {children}
   </FavoritesContext.Provider>
  )
}
