import { IGifResource } from '../../services/gifs';
import { IFavoriteResource } from '../../types';
import { FavoritesTypes } from "../types.d";

export interface IFavoriteAction{
    /**
     * 
     */
    type?: FavoritesTypes;
    /**
     * 
     */
    payload?: IGifResource;
}

export const favoritesReducer=(state:IGifResource, action:IFavoriteAction)=>{
    
    switch (action.type) {
        case FavoritesTypes.addFavorite:
          return[...state as any, action.payload];
        default:
            state;
    }
}