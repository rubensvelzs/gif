export enum FavoritesTypes{
    addFavorite= "[Favorite] Add",
    removeFavorite= "[Favorite] Remove",
    getFavorites= "[Favorite] Get"
}