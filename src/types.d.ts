
export interface IBaseState{
    /**
     * 
     */
    error?: boolean;
    /**
     * 
     */
    loading?:boolean;
}

export interface INavigationProps{
    /**
     * 
     */
    label:string;
    /**
     * 
     */
    link:string;
}

export interface IUserNavigationProps{
    /**
     * Handle navigation action.
     */
    action: ()=> void;
    /**
     * Unique user navigation identifier.
     */
    id: number;
    /**
     * Navigation label.
     */
    label: string;
}

export interface IUserResource{
    /**
     * Logged user's avatar.
     */
    avatar?:string;
    /**
     * Logged user's email.
     */
    email?:string;
    /**
     * Logged user's name.
     */
    name?:string;
}

export interface IFavoriteResource{
    /**
     * 
     */
    id?:string;
    /**
     * 
     */
    images?:{downsized_medium:{url:string}};
    /**
     * 
     */
    username?:string;
    /**
     * 
     */
    title?:string;
}