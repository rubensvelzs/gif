import {Card, Grid, Link, Text } from "@nextui-org/react";
import { useContext } from "react"
import { GifGrid } from "../../components/modules";
import { FavoritesContext } from "../../favorites/context"
import { IGifResource } from "../../services/gifs";
import { BsHeartFill} from 'react-icons/bs';



export const FavoritesPage:React.FC = (props):JSX.Element => {

  const {removeFavorite} = useContext(FavoritesContext);

  const favorites= JSON.parse(localStorage.getItem('favorites') as string);

  const onRemoveFavorite=(favoriteId:string)=>{
    removeFavorite(favoriteId);
    favorites;
  }

  return (

   <Grid.Container  gap={6} justify="center">
   {favorites?.map((item: IGifResource)=>(
        <Grid key={item?.id} md={4}>
          <Card css={{ marginBottom:"12px", width:"100%"}}>
            <Card.Image
              alt="Card image background"
              height={340}
              objectFit="cover"
              src={item?.images?.downsized_medium?.url as string}
              width="100%"
            />
            <Card.Footer>
            <div style={{display:"flex", justifyContent:"space-between", flexDirection:"column"}}>
                <Link as="a" onPress={()=>onRemoveFavorite(item?.id)}>
                  <BsHeartFill color="red" size={22}/>
                </Link>
                <Text size="$sm" h5>{item?.title}</Text>
            </div>
            
            </Card.Footer>
      </Card>     
    </Grid>
    ))} 

   </Grid.Container>

  )
}
