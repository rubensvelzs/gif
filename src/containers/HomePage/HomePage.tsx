import { useEffect, useState } from "react";
import { SearchForm } from "../../components/forms";
import { useForm, useGifPerformance } from "../../hooks";
import { findByName, IGifResource, ISearchFormProps } from "../../services/gifs";
import { GifGrid } from "../../components/modules";



export const HomePage = () => {
const  [gifs, setGifs] = useState<IGifResource[]>([]);

  const {name, handleChange} = useForm<ISearchFormProps>({name:""});  
  const [state, removeError, removeLoading, setError, setLoading]= useGifPerformance();

  const getData = async ()=>{
    try {
    setLoading();
    const gifs = await findByName({q:name, limit:50});
    setGifs(gifs);
    
    removeLoading();
    } catch (error) {
      setError()
    }
  }
  
  return (
    <>
      <SearchForm handleChange={handleChange} onSubmit={getData}/>
      <GifGrid gifs={gifs} performance={state}/>
    </>
  )
}
