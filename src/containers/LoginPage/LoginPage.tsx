import { Button, Container, useTheme } from "@nextui-org/react"
import { useContext } from "react";
import { useNavigate } from "react-router-dom"
import { AuthContext } from "../../auth/context";


export const LoginPage = () => {
  const navigate= useNavigate();

  const {login} = useContext(AuthContext)

  const lastPath = localStorage.getItem('lastPath');

  const onLogin = ()=>{
    login({
    avatar:"https://hips.hearstapps.com/hmg-prod.s3.amazonaws.com/images/kunal-nayyar-1605874474.jpg",
    name: "Ruben",
    email:"rubensvelzs@gmail.com"
    });
    navigate(lastPath as string, {replace:true});
  }

  return (
   <>
    <Button auto color="secondary" ghost 
    css={{display:"flex", justifyContent:"center"}}
    onPress={onLogin}
    >
      Login
    </Button>
   
   </>
  )
}
