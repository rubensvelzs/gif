import { useContext, useMemo } from 'react';
import { Navigate } from 'react-router-dom';
import { AuthContext } from '../../../auth/context';
import {Header} from '../../../components/elements'
import { useNavigation } from '../../../hooks'
import { RouteTree } from './RouteTree';

export const Root:React.FC = ():JSX.Element => {
  const userNavigation = useNavigation();
  const {user} = useContext(AuthContext);

  return (
    <div style={{display:"flex", flexDirection:"column"}}>      
        <Header user={user} userNavigation={userNavigation}/>
        <RouteTree/>
    </div>
  )
}
