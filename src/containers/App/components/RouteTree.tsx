import {BrowserRouter, Routes, Route} from 'react-router-dom';
import { FavoritesPage } from '../../FavoritesPage';
import { HomePage } from '../../HomePage';

export const RouteTree = () => {
  return (
    <div>
      <Routes>
            <Route path="/" element={<HomePage/>} />
            <Route path="/favorites" element={<FavoritesPage/>}/>
      </Routes>
    </div>
        
  )
}
