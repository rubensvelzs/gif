import { ReactElement, ReactNode, useContext, useMemo } from "react"
import { Navigate, useLocation } from "react-router-dom";
import { AuthContext } from "../../../auth/context";

export interface IPrivateRouteProps{
    /**
     * 
     */
    children: ReactElement;
}

export const PrivateRoute:React.FC<IPrivateRouteProps> = (props):JSX.Element => {
    const {children} = props;
    const {user} = useContext(AuthContext);
    const {pathname}= useLocation();

    const lastPath= pathname;
    useMemo(()=>localStorage.setItem('lastPath', lastPath),[lastPath]);
    
   return user?(
    children
   ):(<Navigate to="/login"/>)
}
