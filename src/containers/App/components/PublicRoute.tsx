import { ReactElement, ReactNode, useContext } from "react"
import { Navigate } from "react-router-dom";
import { AuthContext } from "../../../auth/context";

export interface IPublicRouteProps{
    /**
     * 
     */
    children: ReactElement;
}

export const PublicRoute:React.FC<IPublicRouteProps> = (props):JSX.Element => {
    const {children} = props;
    const {user} = useContext(AuthContext);
    
   return !user?(
    children
   ):(<Navigate to="/"/>)
}
