import { useContext } from 'react';
import {BrowserRouter,Routes, Route, Navigate} from 'react-router-dom';
import { AuthContext, AuthProvider } from '../../auth/context';
import { LoginPage } from '../LoginPage';
import { PrivateRoute } from './components/PrivateRoute';
import { PublicRoute } from './components/PublicRoute';
import { Root } from './components/Root';

export const GifApp:React.FC = ():JSX.Element => {

  return(
    <BrowserRouter>
      <Routes>
        <Route path="/login" element={<PublicRoute>
          <LoginPage/>
        </PublicRoute>}/>

        <Route path="/*" element={<PrivateRoute>
          <Root/>
        </PrivateRoute>} />
      </Routes>
    </BrowserRouter>
  )
}
