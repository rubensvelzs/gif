
export interface IGifQueryParams{
    /**
     * 
     */
    limit?: number;
    /**
     * 
     */
    q?: string;
}


export interface IGifResource{
    /**
     * Gif's unique ID.
     */
    id: string;
    /**
     * Gif's image URL.
     */
    images:{downsized_medium:{url:string}};
    /**
     * Gif's title.
     */
    title: string;
    /**
     * Gif's username.
     */
    username: string;
}

export interface ISearchFormProps{
    /**
     * 
     */
    name?: string;
}