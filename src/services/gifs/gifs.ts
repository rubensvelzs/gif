import { IGifQueryParams, IGifResource } from './types';

export const findByName = async (p: IGifQueryParams):Promise<IGifResource[]> =>{
    const resp = await fetch(`${import.meta.env.VITE_APP_API_URL}/search?api_key=${import.meta.env.VITE_APP_API_KEY_URL}&q=${p?.q}&limit=${p?.limit}`);
    const {data} = await resp.json();

    const gifs: IGifResource[] = data?.map((item: IGifResource) =>({
        id: item?.id,
        images : item?.images,
        title : item?.title,
        username: item?.username
      }))
  
    return gifs;
  }