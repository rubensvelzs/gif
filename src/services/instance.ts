
export const instance = async(p:string)=>{
    const resp = await fetch(`${process.env.REACT_APP_API_URL}/${p}?${process.env.REACT_APP_API_KEY_URL}`);
    const {data} = await resp.json();

    return data;
}