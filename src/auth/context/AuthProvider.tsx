import { ReactNode, useReducer } from "react";
import { IUserResource } from "../../types";
import { authReducer } from "../reducers";
import { AuthenticationTypes } from "../types.d";
import { AuthContext } from "./AuthContext";

export interface IAuthProviderProps{
    /**
     * 
     */
    children: ReactNode;
}

export const AuthProvider:React.FC<IAuthProviderProps> = (props):JSX.Element => {
    const {children}= props;

    const init = ()=>{
      const user = JSON.parse(localStorage.getItem('user') as string);
      return {
        logged: !!user,
        user
      }
    }

    const [state, dispatch] = useReducer(authReducer, null, init);
    
    const login=(user: IUserResource)=>{
      const action={
        type: AuthenticationTypes.login,
        payload: user
      }
      localStorage.setItem('user',JSON.stringify(user));
      dispatch(action);
    }

    const logout=()=>{
      const action={
        type:AuthenticationTypes.logout,
      }
      localStorage.removeItem('user');
      dispatch(action);
    }

  return (
    <AuthContext.Provider value={{
      ...state,
      login,
      logout
    }}>
        {children}
    </AuthContext.Provider>
  )
}
