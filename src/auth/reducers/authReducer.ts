import { AuthenticationTypes } from "../types.d";


export const authReducer = (state: any, action:any)=>{
    switch (action.type) {
        case AuthenticationTypes.login:
            return {
                ...state,
                logged:true,
                user: action.payload
            }
        case AuthenticationTypes.logout:
            return{
                logged:false
            }
        default:
            return state
    }
}