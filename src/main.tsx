import React from 'react';
import ReactDOM from 'react-dom/client'
import { GifApp } from './containers/App';
import { NextUIProvider } from '@nextui-org/react';
import { darkTheme } from '../themes';
import { AuthProvider } from './auth/context';
import { FavoritesProvider } from './favorites/context';


ReactDOM.createRoot(document.getElementById('root') as HTMLElement).render(
  <React.StrictMode>
      <NextUIProvider theme={darkTheme}>
        <AuthProvider>
          <FavoritesProvider>
             <GifApp  />
          </FavoritesProvider>
        </AuthProvider>
      </NextUIProvider>
  </React.StrictMode>,
)
